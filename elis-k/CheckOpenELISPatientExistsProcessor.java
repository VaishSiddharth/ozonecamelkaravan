import org.apache.camel.BindToRegistry;
import org.apache.camel.Configuration;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.hl7.fhir.r4.model.Patient;
import java.util.*;
import ca.uhn.fhir.context.FhirContext;
import java.io.IOException;
import okhttp3.OkHttpClient;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Configuration
@BindToRegistry("CheckOpenELISPatientExistsProcessor")
public class CheckOpenELISPatientExistsProcessor implements Processor {

    public void process(Exchange exchange) throws Exception {
        Patient patient = exchange.getIn().getBody(Patient.class);
        FhirContext ctx = FhirContext.forR4();
        if (doesPatientExistsInOpenELIS(patient)) {
            exchange.getIn().setBody("");
            return;
        }
        String json = ctx.newJsonParser().setPrettyPrint(true).encodeResourceToString(patient);
        exchange.getIn().setBody(json);
    }

    private boolean doesPatientExistsInOpenELIS(Patient patient) {
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        Request request = new Request.Builder().url("http://192.168.29.135:9091/fhir/Patient").get().build();
        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful() && response.body() != null) {
                String body = response.body().string();
                if (body.contains(patient.getIdentifier().get(0).getId())) {
                    return true;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return false;
    }
}