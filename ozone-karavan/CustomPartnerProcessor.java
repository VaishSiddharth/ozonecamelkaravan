import org.apache.camel.BindToRegistry;
import org.apache.camel.Configuration;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.hl7.fhir.r4.model.*;
import java.util.*;

@Configuration
@BindToRegistry("CustomPartnerProcessor")
public class CustomPartnerProcessor implements Processor {

    public void process(Exchange exchange) throws Exception {
       Patient patient = exchange.getIn().getBody(Patient.class);

       Partner partner = new Partner();
       partner.setPartnerRef(patient.getIdPart());
       partner.setPartnerActive(patient.getActive());
       String patientName = getPatientName(patient).orElse("");
       String patientIdentifier = getPreferredPatientIdentifier(patient).orElse("");
       partner.setPartnerComment(patientIdentifier);
       partner.setPartnerName(patientName);

       exchange.getIn().setBody(partner);
    }

    protected Optional<String> getPreferredPatientIdentifier(Patient patient) {
    return patient.getIdentifier().stream().filter(identifier -> identifier.getUse() == Identifier.IdentifierUse.OFFICIAL).findFirst().map(Identifier::getValue);
    }

    protected Optional<String> getPatientName(Patient patient) {
    return patient.getName().stream().findFirst().map(name -> name.getGiven().get(0) + " " + name.getFamily());
    }
}