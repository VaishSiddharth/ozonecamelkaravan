import org.apache.camel.BindToRegistry;
import org.apache.camel.Configuration;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.hl7.fhir.r4.model.*;
import java.util.*;
import ca.uhn.fhir.context.FhirContext;

@Configuration
@BindToRegistry("FhirPatientProcessor")
public class FhirPatientProcessor implements Processor {

    public void process(Exchange exchange) throws Exception {
        String body = exchange.getIn().getBody(String.class);
        FhirContext ctx = FhirContext.forR4();
        Bundle bundle =(Bundle) ctx.newJsonParser().parseResource(Bundle.class, body);
        List<Bundle.BundleEntryComponent> entries = bundle.getEntry();

        Patient patient = null;

        for (Bundle.BundleEntryComponent entry : entries) {
            Resource resource = entry.getResource();
            if (resource instanceof Patient) {
                patient = (Patient) resource;
                break;
            }
        }

        Partner partner = new Partner();
        partner.setPartnerRef(patient.getIdPart());
        partner.setPartnerActive(patient.getActive());
        String patientName = getPatientName(patient).orElse("");
        String patientIdentifier = getPreferredPatientIdentifier(patient).orElse("");
        partner.setPartnerComment(patientIdentifier);
        partner.setPartnerName(patientName);
        exchange.getIn().setBody(partner);
    }

    protected Optional<String> getPreferredPatientIdentifier(Patient patient) {
    return patient.getIdentifier().stream().filter(identifier -> identifier.getUse() == Identifier.IdentifierUse.OFFICIAL).findFirst().map(Identifier::getValue);
    }

    protected Optional<String> getPatientName(Patient patient) {
    return patient.getName().stream().findFirst().map(name -> name.getGiven().get(0) + " " + name.getFamily());
    }
}