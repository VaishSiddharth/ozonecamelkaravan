import org.apache.camel.BindToRegistry;
import org.apache.camel.Configuration;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.hl7.fhir.r4.model.Patient;
import ca.uhn.fhir.context.FhirContext;

@Configuration
@BindToRegistry("CustomPatientProcessor")
public class CustomPatientProcessor implements Processor {

    public void process(Exchange exchange) throws Exception {
        String body = exchange.getIn().getBody(String.class);
        FhirContext ctx = FhirContext.forR4();
        Patient patient =(Patient) ctx.newJsonParser().parseResource(Patient.class, body);
        exchange.getIn().setBody(patient);
    }
}