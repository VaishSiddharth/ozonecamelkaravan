import org.apache.camel.BindToRegistry;
import org.apache.camel.Configuration;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Arrays;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;

@Configuration
@BindToRegistry("CreatePartnerProcessor")
public class CreatePartnerProcessor implements Processor {

    public void process(Exchange exchange) throws Exception {
        Partner partner = exchange.getIn().getBody(Partner.class);
        OdooClient odooClient = new OdooClient();
        Integer result = odooClient.create("res.partner", Arrays.asList(convertObjectToMap(partner)));
        exchange.getIn().setBody(result);
    }

    public static Map<String, Object> convertObjectToMap(Object object) {
        try {
            Map<String, Object> map = new HashMap<>();
            Field[] fields = object.getClass().getDeclaredFields();

            for (Field field : fields) {
                field.setAccessible(true);
                if (field.isAnnotationPresent(JsonProperty.class)) {
                    JsonProperty jsonProperty = field.getAnnotation(JsonProperty.class);
                    String key = jsonProperty.value();
                    Object value = field.get(object);
                    map.put(key, value);
                }
            }
            return map;
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}