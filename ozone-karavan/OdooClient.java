import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import lombok.extern.slf4j.Slf4j;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

@Slf4j
public class OdooClient {

    private String url = "https://erp.ozone-dev.mekomsolutions.net";

    private String database = "odoo";

    private String username = "admin";

    private String password = "admin";

    private Integer uid;

    private XmlRpcClientConfigImpl xmlRpcClientConfig;

    private XmlRpcClient client;

    private static final String SERVER_OBJECT_URL = "%s/xmlrpc/2/object";

    private static final String SERVER_COMMON_URL = "%s/xmlrpc/2/common";

    public OdooClient() {
    }

    public void init() {
        if (xmlRpcClientConfig == null) {
            try {
                xmlRpcClientConfig = new XmlRpcClientConfigImpl();
                xmlRpcClientConfig.setEnabledForExtensions(true);
                xmlRpcClientConfig.setServerURL(new URL(String.format(SERVER_OBJECT_URL, url)));
            } catch (MalformedURLException e) {
                log.error("Error occurred while building odoo server url {} error {}", url, e.getMessage());
                throw new RuntimeException(e);
            }
        }
        if (client == null) {
            client = new XmlRpcClient();
            client.setConfig(xmlRpcClientConfig);
        }
        // authenticate
        if (uid == null) {
            try {
                XmlRpcClientConfigImpl xmlRpcClientCommonConfig = new XmlRpcClientConfigImpl();
                xmlRpcClientCommonConfig.setServerURL(new URL(String.format(SERVER_COMMON_URL, url)));
                uid = (Integer) client.execute(
                        xmlRpcClientCommonConfig,
                        "authenticate",
                        asList(database, username, password, emptyMap()));
            } catch (XmlRpcException | MalformedURLException e) {
                log.error("Cannot authenticate to Odoo server error: {}", e.getMessage());
                throw new RuntimeException("Cannot authenticate to Odoo server");
            }
        }
    }

    public Integer create(String model, List<Map<String, Object>> dataParams) {
        init();

        try {
            return (Integer) client.execute(
                    "execute_kw",
                    asList(database, uid, password, model, "create", dataParams));
        } catch (XmlRpcException e) {
            log.error("Error occurred while creating in odoo server error {}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public Boolean write(String model, List<Object> dataParams) {
        init();

        try {
            return (Boolean)
                    client.execute("execute_kw", asList(database, uid, password, model, "write", dataParams));
        } catch (XmlRpcException e) {
            log.error("Error occurred while writing to odoo server error {}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public Boolean delete(String model, List<Object> dataParams) {
        init();

        try {
            return (Boolean) client.execute(
                    "execute_kw", asList(database, uid, password, model, "unlink", dataParams));
        } catch (XmlRpcException e) {
            log.error("Error occurred while deleting from odoo server error {}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public Object[] searchAndRead(String model, List<Object> criteria, List<String> fields) {
        init();

        try {
            List<Object> params = new ArrayList<>();
            params.add(database);
            params.add(uid);
            params.add(password);
            params.add(model);
            params.add("search_read");
            params.add(singletonList(criteria));
            if (fields != null) {
                params.add(singletonMap("fields", fields));
            }

            return (Object[]) client.execute("execute_kw", params);
        } catch (XmlRpcException e) {
            log.error("Error occurred while searchAndRead from odoo server error {}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public Object[] search(String model, List<Object> criteria) {
        init();

        try {
            return (Object[]) client.execute(
                    "execute_kw",
                    asList(database, uid, password, model, "search", singletonList(singletonList(criteria))));
        } catch (XmlRpcException e) {
            log.error("Error occurred while searching from odoo server error {}", e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
